
#include "TDS_StateEffect.h"

#include "NiagaraFunctionLibrary.h"
#include "GameFramework/Character.h"
#include "TDS/Character/TDS_HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS/Character/TDSCharacter.h"

//Base Class
bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
		myInterface->AddEffect(this);
	
	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
		myInterface->RemoveEffect(this);
	
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
		this->ConditionalBeginDestroy();
}

//Execute Once Class
bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealth(Power);
		}
	}
	
	DestroyObject();
}

//Execute Timer Class
bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);
	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(),
			NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}
	
	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealth(Power);
		}
	}
}

//Invincibility Effect Class
bool UTDS_StateEffect_Invincibility::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	Execute();
	return true;
}

void UTDS_StateEffect_Invincibility::DestroyObject()
{
	UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));
	if (myHealthComp)
	{
		myHealthComp->OnEndInvincible.Broadcast();
	}
	Super::DestroyObject();
}

void UTDS_StateEffect_Invincibility::Execute()
{
	if (myActor)
	{
		UTDS_HealthComponent* myHealthComp = Cast<UTDS_HealthComponent>(myActor->GetComponentByClass(UTDS_HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->OnInvincible.Broadcast();
		}
	}
}




