// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

#include <ThirdParty/Vulkan/Include/vulkan/vulkan_core.h>

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;
}

bool UTDSGameInstance::GetDropWeaponInfoByName(FName NameWeapon, FDropWeapon& OutInfo)
{
	bool bIsFind = false;
	
	if (DropWeaponInfoTable)
	{
		FDropWeapon* DropWeaponInfoRow;
		TArray<FName>RowNames = DropWeaponInfoTable->GetRowNames();
		
		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropWeaponInfoRow = DropWeaponInfoTable->FindRow<FDropWeapon>(RowNames[i],"");
			if (DropWeaponInfoRow->WeaponInfo.NameItem == NameWeapon)
			{
				OutInfo = (*DropWeaponInfoRow);	
				bIsFind = true;
			}
			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetDropWeaponInfoByName - DropWeaponInfoTable -NULL"));
	}

	return bIsFind;
}
