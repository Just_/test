// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS/FunctionLibrary/Types.h"
#include "TDSInventoryComponent.generated.h"

/*DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon,
	FName, WeaponIdName,
	FAdditionalWeaponInfo, WeaponAdditionalInfo,
	int32, NewCurrentIndexWeapon);*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponOutOfAmmo, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvailable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewInfo);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDSInventoryComponent();

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
		FOnSwitchWeapon OnSwitchWeapon;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoChange OnAmmoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponOutOfAmmo OnWeaponOutOfAmmo;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAmmoAvailable OnWeaponAmmoAvailable;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnUpdateWeaponSlots OnUpdateWeaponSlots;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FAmmoSlot> AmmoSlots;
	
	int32 MaxSlotsWeapon = 0;
	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo, bool bIsForward);
	bool SwitchWeaponByKeyInput (int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo);
	
	FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 IndexWeapon);
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);
	
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		bool GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType &WeaponType);
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType &WeaponType);
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		FName GetWeaponNameByIndexSlot(int32 IndexSlot);
	
	UFUNCTION(BlueprintCallable, Category = "Interface")
		void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo);
	
	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AvailableAmmoForWeapon);

	//interface pickup actors
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeAmmo(EWeaponType AmmoType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeWeapon(FName WeaponName, int32 &FreeSlot);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckHaveSameWeapon(FName WeaponName);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckIsAnyEmptySlotsAvailable();
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCurrentSlotZeroByName(FName WeaponName);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool SwitchWeaponToInventory(FWeaponSlot NewWeaponInfo, int32 IndexSlot, FDropWeapon &DropWeaponInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool GetDropCurrentWeaponInfo(int32 IndexSlot, FDropWeapon &DropWeaponInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		void DropWeaponByIndex(int32 ByIndex, FDropWeapon &DropItemInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool TryGetWeaponToInventory(FWeaponSlot NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "Inv")
		void InitInventory(TArray<FWeaponSlot> NewWeaponSlotsInfo, TArray<FAmmoSlot> NewAmmoSlotsInfo);
	
};
