
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FunctionLibrary/Types.h"
#include "TDS/WeaponDefault.h"
#include "TDSInventoryComponent.h"
#include "TDSCharacter_HealthComponent.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "TDS/StateEffects/TDS_StateEffect.h"

#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATDSCharacter();

	
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;


	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
		class UTDSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Health, meta = (AllowPrivateAccess = "true"))
		class UTDSCharacter_HealthComponent* CharacterHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:

	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	
	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeedInfo;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsMovingForward;
	UPROPERTY()
		float AngleError;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		TArray<UAnimMontage*> DealthMontages;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilitySystem")
		TSubclassOf<UTDS_StateEffect> AbilityEffect;

	//Inputs
	UFUNCTION()
		void InputAxisY(float Value);

	UFUNCTION()
		void InputAxisX(float Value);

	UFUNCTION()
		void InputAttackPressed();

	UFUNCTION()
		void InputAttackReleased();

	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	UFUNCTION(BlueprintCallable)
		void TryEnableAbility();

	UFUNCTION(BlueprintCallable)
		void DropCurrentWeapon();

	template<int32 id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(id);
	}

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UDecalComponent* CurrentCursor = nullptr;


	//Weapon
	AWeaponDefault* CurrentWeapon = nullptr;

	//for demo
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	//Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//Func
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION()
		FVector TraceForward() const;
	UFUNCTION()
		FVector TraceVelocity() const;

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo/*, int32 NewCurrentIndexWeapon*/);

	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* CharAnim, UAnimMontage* WeaponAnim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* CharAnim, UAnimMontage* WeaponAnim);

	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* CharAnim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* CharAnim);

	//inventory
	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();
	bool TrySwitchWeaponToIndexByKeyInput(int32 index);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;
	UPROPERTY(BlueprintReadOnly)
		EWeaponType CurrentWeaponType = EWeaponType::RifleType;

	//Interface
	virtual EPhysicalSurface GetSurfaceType() override;
	//End Interface

	//Effects
	TArray<UTDS_StateEffect*> Effects;
	virtual TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
	virtual void RemoveEffect(UTDS_StateEffect* EffectToRemove) override;
	virtual void AddEffect(UTDS_StateEffect* NewEffect) override;
	
	UFUNCTION(BlueprintCallable)
		void CharDead();
	FTimerHandle TimerHandle_RagdollTimer;
	void EnableRagdoll();
	
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();
	
};

