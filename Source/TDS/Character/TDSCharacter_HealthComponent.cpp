
#include "TDSCharacter_HealthComponent.h"
#include "Kismet/KismetSystemLibrary.h"


void UTDSCharacter_HealthComponent::ChangeHealth(float ChangeValue)
{
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeValue = ChangeValue * DamageCoefShield;
		ChangeShield(ChangeValue);
		if (Shield <= 0.0f)
		{
			//FX
			//UKismetSystemLibrary::PrintString(GetWorld(), "Shield Destroyed");
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CooldownShieldTimer);
				GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldDestroyedTimer, this,
												   &UTDSCharacter_HealthComponent::ShieldUp,
												   ShieldUpRate, false);
			}
			
			OnShieldChangeState.Broadcast(true);
			UE_LOG(LogTemp, Warning, TEXT("UTDSCharacter_HealthComponent::ChangeHealth - Shield destroyed"));
		}
	}
	else
	{
		Super::ChangeHealth(ChangeValue);
	}
}

float UTDSCharacter_HealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharacter_HealthComponent::ChangeShield(float ChangeValue)
{
	Shield += ChangeValue;
	OnShieldChange.Broadcast(Shield, ChangeValue);
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}
	
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownShieldTimer, this,
			&UTDSCharacter_HealthComponent::CooldownShieldEnd,
			CooldownShieldRecharge, false);
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRechargeRateTimer);
	}
}

void UTDSCharacter_HealthComponent::CooldownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRechargeRateTimer, this,
		                                       &UTDSCharacter_HealthComponent::RecoveryShield,
		                                       ShieldRecoveryRate, true);
		OnShieldChangeState.Broadcast(false);
	}
}


void UTDSCharacter_HealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp += ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRechargeRateTimer);
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}

void UTDSCharacter_HealthComponent::ShieldUp()
{
	ChangeShield(0);
}
