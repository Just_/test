// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventoryComponent.h"
#include "TDS/TDSGameInstance.h"
#include "list"
#include "TDSCharacter.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTDSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	//bool bIsEmptySlot = false;
	int8 CorrectIndex = ChangeToIndex;
	int32 i = 0;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
		CorrectIndex = 0;
	else
	{
		if (ChangeToIndex < 0)
		{
			int8 k = WeaponSlots.Num() - 1;
			bool bIsFind2 = false;
			while (k >= 0 && !bIsFind2)
			{
				if (!WeaponSlots[k].NameItem.IsNone())
				{
					CorrectIndex = k;
					bIsFind2 = true;
				}
				k--;
			}
		}
	}

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	//int32 NewCurrentIndex = 0;

	if (bIsForward)
	{
		bool bIsFind = false;
		int8 j = OldIndex + 1;
		while (j != WeaponSlots.Num() && !bIsFind)
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				CorrectIndex = j;
				bIsFind = true;
			}
			else
			{
				CorrectIndex = 0;
			}
			j++;
		}
	}
	else
	{
		bool bIsFind = false;
		int8 j = OldIndex - 1;
		while (j >= 0 && !bIsFind)
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				CorrectIndex = j;
				bIsFind = true;
			}
			else
			{
				int8 k = WeaponSlots.Num() - 1;
				bool bIsFind2 = false;
				while (k >= 0 && !bIsFind2)
				{
					if (!WeaponSlots[k].NameItem.IsNone())
					{
						CorrectIndex = k;
						bIsFind2 = true;
					}
					k--;
				}
			}
			j--;
		}
	}

	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		if (i == CorrectIndex)
		{
			if (NewIdWeapon.IsNone())
			{
				NewIdWeapon = WeaponSlots[i].NameItem;
				NewAdditionalInfo = WeaponSlots[i].AdditionalInfo;
				bIsSuccess = true;
			}
		}	
		i++;
	}
	
	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);
	}
	return bIsSuccess;
}

bool UTDSInventoryComponent::SwitchWeaponByKeyInput(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	bool bIsSuccess = false;
	
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	if (WeaponSlots.IsValidIndex(ChangeToIndex) && !WeaponSlots[ChangeToIndex].NameItem.IsNone() && NewIdWeapon.IsNone())
	{
		NewIdWeapon = WeaponSlots[ChangeToIndex].NameItem;
		NewAdditionalInfo = WeaponSlots[ChangeToIndex].AdditionalInfo;
		bIsSuccess = true;
	}
	
	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UTDSInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;

	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (/*WeaponSlots[i].IndexSlot*/ i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::GetAdditionalInfoWeapon - No Found Weapon With index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::GetAdditionalInfoWeapon - Not Correct Weapon index - %d"), IndexWeapon);
	
	return result;
}

int32 UTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;

	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i /*WeaponSlots[i].IndexSlot*/;
		}
		i++;
	}
	return result;
}

bool UTDSInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			myGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;
		}
	}
	return bIsFind;
}

bool UTDSInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		myGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	return bIsFind;
}

FName UTDSInventoryComponent::GetWeaponNameByIndexSlot(int32 IndexSlot)
{
	FName WeaponName;

	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		WeaponName = WeaponSlots[IndexSlot].NameItem;
	}
	
	return WeaponName;
}

void UTDSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (/*WeaponSlots[i].IndexSlot*/ i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAdditionalInfoWeapon - No Dound Weapon With index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAdditionalInfoWeapon - Not Correct Weapon index - %d"), IndexWeapon);
}

void UTDSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int i = 0;
	while (i<AmmoSlots.Num() && !bIsFind)
	{
		if(AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += CoutChangeAmmo;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
				AmmoSlots[i].Cout =  AmmoSlots[i].MaxCout;
			
			OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);

			bIsFind = true;
		}
		i++;
	}
}

bool UTDSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AvailableAmmoForWeapon = AmmoSlots[i].Cout;
			if (AmmoSlots[i].Cout > 0)
			{
				OnWeaponAmmoAvailable.Broadcast(TypeWeapon);
				
				return true;
			}
		}
			
		i++;
	}
	
	OnWeaponOutOfAmmo.Broadcast(TypeWeapon);
	
	return false;
}

bool UTDSInventoryComponent::CheckCanTakeWeapon(FName WeaponName, int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	bool bHaveSameWeapon = false;
	int8 j = 0;
	while (j < WeaponSlots.Num())
	{
		if (WeaponSlots[j].NameItem == WeaponName)
		{
			bHaveSameWeapon = true;
		}
		j++;
	}

	if (!bHaveSameWeapon)
	{
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFreeSlot)
		{
			if (WeaponSlots[i].NameItem.IsNone())
			{
				FreeSlot = i;
				bIsFreeSlot = true;
			}
			i++;
		}
	}
	return bIsFreeSlot;
}

bool UTDSInventoryComponent::CheckHaveSameWeapon(FName WeaponName)
{
	bool bHaveSameWeapon = false;
	int8 i = 0;
	while (i < WeaponSlots.Num())
	{
		if (WeaponSlots[i].NameItem == WeaponName)
		{
			bHaveSameWeapon = true;
		}
		i++;
	}
	return bHaveSameWeapon;
}

bool UTDSInventoryComponent::CheckIsAnyEmptySlotsAvailable()
{
	int8 i = 0;
	int8 AvailableEmptySlots = 0;
	while (i < WeaponSlots.Num())
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			AvailableEmptySlots++;
		}
		i++;
	}
	if (AvailableEmptySlots >= 1)
	{
		return true;
	}
	return false;
}

bool UTDSInventoryComponent::CheckCurrentSlotZeroByName(FName WeaponName)
{
	bool result = false;
	if (WeaponName == FName(TEXT("Pistol")))
	{
		result = true;
	}
	return result;
}

bool UTDSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
			result = true;
		i++;
	}
	
	return result;
}

bool UTDSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeaponInfo, int32 IndexSlot, FDropWeapon &DropWeaponInfo)
{
	bool result = false;
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropCurrentWeaponInfo(IndexSlot, DropWeaponInfo))
	{
		WeaponSlots[IndexSlot] = NewWeaponInfo;
		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeaponInfo);
		OnSwitchWeapon.Broadcast(NewWeaponInfo.NameItem, NewWeaponInfo.AdditionalInfo);
		result = true;
	}
	return result;
}

bool UTDSInventoryComponent::GetDropCurrentWeaponInfo(int32 IndexSlot, FDropWeapon &DropWeaponInfo)
{
	bool result = false;
	FName DropWeaponName = GetWeaponNameByIndexSlot(IndexSlot);

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		result = myGI->GetDropWeaponInfoByName(DropWeaponName, DropWeaponInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
			DropWeaponInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
	}
	return result;
}

void UTDSInventoryComponent::DropWeaponByIndex(int32 ByIndex, FDropWeapon& DropItemInfo)
{
	FWeaponSlot EmptyWeaponSlot;

	if (WeaponSlots[ByIndex].NameItem != "Pistol")
	{
		bool bIsCanDrop = false;
		int8 i = 0;
		int8 AvailableWeaponsNum = 0;
		while (i < WeaponSlots.Num())
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				AvailableWeaponsNum++;
			}
			i++;
		}
		
		if (AvailableWeaponsNum > 1)
			bIsCanDrop = true;
		
		if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropCurrentWeaponInfo(ByIndex, DropItemInfo))
		{
			GetDropCurrentWeaponInfo(ByIndex, DropItemInfo);
			
			int8 j = ByIndex + 1;
			if (j < WeaponSlots.Num())
			{
				if (AvailableWeaponsNum > 2)
				{
					bool bIsFindWeapon = false;
					while (!bIsFindWeapon)
					{
						if (!WeaponSlots[j].NameItem.IsNone())
						{
							OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo);
							bIsFindWeapon = true;
						}
						j++;
					}
				}
			}
			else
			{
				if (!WeaponSlots[0].NameItem.IsNone())
				{
					OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo);
				}
			}
			
			if (AvailableWeaponsNum <= 2)
			{
				if (!WeaponSlots[0].NameItem.IsNone())
				{
					OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo);
				}
			}

			WeaponSlots[ByIndex] = EmptyWeaponSlot;
			if (GetOwner()->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
			{
				ITDS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
			}
			OnUpdateWeaponSlots.Broadcast(ByIndex, EmptyWeaponSlot);
		}
	}
}

bool UTDSInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(NewWeapon.NameItem, IndexSlot))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			WeaponSlots[IndexSlot] = NewWeapon;
			OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
			return true;
		}
	}
	return  false;
}

void UTDSInventoryComponent::InitInventory(TArray<FWeaponSlot> NewWeaponSlotsInfo, TArray<FAmmoSlot> NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;
	//Find init weaponsSlots and First Init Weapon
	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
			//	if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
				//	WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
			}
		}
	}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo/*, 0*/);
	}
}
