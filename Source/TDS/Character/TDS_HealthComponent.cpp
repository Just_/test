
#include "TDS_HealthComponent.h"

UTDS_HealthComponent::UTDS_HealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	
}

void UTDS_HealthComponent::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void UTDS_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
}

float UTDS_HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDS_HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

//+Change value = heal, -ChangeValue = damage
void UTDS_HealthComponent::ChangeHealth(float ChangeValue)
{
	ChangeValue = ChangeValue * DamageCoef;
	Health += ChangeValue;
	OnHealthChange.Broadcast(Health, ChangeValue);
	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health <= 0.0f)
		{
			OnDead.Broadcast();
		}
	}
}

