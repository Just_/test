
#pragma once

#include "CoreMinimal.h"
#include "TDS_HealthComponent.h"
#include "TDSCharacter_HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, ChangeVale);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShieldChangeState, bool, bIsShieldDestroyed);

UCLASS()
class TDS_API UTDSCharacter_HealthComponent : public UTDS_HealthComponent
{
	GENERATED_BODY()

public:
	
	FTimerHandle TimerHandle_CooldownShieldTimer;
	FTimerHandle TimerHandle_ShieldRechargeRateTimer;
	FTimerHandle TimerHandle_ShieldDestroyedTimer;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChangeState OnShieldChangeState;

protected:
	float Shield = 100.0f;
	
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CooldownShieldRecharge = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldUpRate = 7.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float DamageCoefShield = 1.0f;
	
	virtual void ChangeHealth(float ChangeValue) override;

	float GetCurrentShield();
	void CooldownShieldEnd();
	void ChangeShield(float ChangeValue);
	void RecoveryShield();
	void ShieldUp();
};
