// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplode(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplode(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > ProjectileSetting.Grenade_TimeToExplose)
		{
			//Explode
			Explode();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	//TimerEnabled = true;
	Explode();
}

void AProjectileDefault_Grenade::Explode()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.Grenade_DamageInnerRadius,
		ProjectileSetting.Grenade_DamageOuterRadius,
		5,
		NULL, IgnoredActor, this, nullptr);
	if (ShowDebug == true)
	{
		DrawDebugSphere(GetWorld(), BulletMesh->GetComponentLocation(), ProjectileSetting.Grenade_DamageInnerRadius, 32, FColor::Blue, false, 5.f, (uint8)'\000', 0.5f);
		DrawDebugSphere(GetWorld(), BulletMesh->GetComponentLocation(), 
			((ProjectileSetting.Grenade_DamageOuterRadius - ProjectileSetting.Grenade_DamageInnerRadius)/2 + ProjectileSetting.Grenade_DamageInnerRadius)
			, 32, FColor::Green, false, 5.f, (uint8)'\000', 0.5f);
		DrawDebugSphere(GetWorld(), BulletMesh->GetComponentLocation(), ProjectileSetting.Grenade_DamageOuterRadius, 32, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
	}

	this->Destroy();
}