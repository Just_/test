
#include "TDS_IGameActor.h"

#include "Interfaces/ITargetDevice.h"


EPhysicalSurface ITDS_IGameActor::GetSurfaceType()
{
	return SurfaceType_Default;
}

TArray<UTDS_StateEffect*> ITDS_IGameActor::GetAllCurrentEffects()
{
	return Effect;
}

void ITDS_IGameActor::RemoveEffect(UTDS_StateEffect* EffectToRemove)
{
	Effect.Remove(EffectToRemove);
}

void ITDS_IGameActor::AddEffect(UTDS_StateEffect* NewEffect)
{
	Effect.Add(NewEffect);
}
