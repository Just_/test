
#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS/FunctionLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TDS/StateEffects/TDS_StateEffect.h"
#include "TDS_IGameActor.generated.h"

UINTERFACE(MinimalAPI)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

class TDS_API ITDS_IGameActor
{

	GENERATED_BODY()

public:
	
	virtual EPhysicalSurface GetSurfaceType();
	TArray<UTDS_StateEffect*> Effect;
	virtual TArray<UTDS_StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_StateEffect* EffectToRemove);
	virtual void AddEffect(UTDS_StateEffect* NewEffect);

	//inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropWeapon DropItemInfo);
};

	
